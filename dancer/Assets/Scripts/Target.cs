﻿using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    [SerializeField] private InputManager.KeysList key;
    [SerializeField] private TargetManager manager;
    [SerializeField] private AudioClip successClip;
    [SerializeField] private AudioClip failureClip;
    [SerializeField] private SpriteRenderer fillSpriteRenderer;

    private List<Block> blocks = new List<Block>();


    private void Start()
    {

        bool biggerTarget = PlayerPrefs.GetInt("BiggerTarget", 0) == 1;

        if (biggerTarget)
        {
            BoxCollider2D collider = GetComponent<BoxCollider2D>();
            collider.size = new Vector2(collider.size.x, collider.size.y * 2);
        }
    }

    public void PlaySound(AudioClip audioClip)
    {
        manager.NotifySuccess(successClip);
    }

    private void Update()
    {
        if (!manager.Active)
        {
            blocks.Clear();
            fillSpriteRenderer.gameObject.SetActive(false);
            return;
        }

        if (blocks.Count == 0)
        {
            fillSpriteRenderer.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(InputManager.Instance.GetKey(key)) && blocks.Count == 0)
        {
            manager.NotifyFailure(failureClip);
        }
        if (Input.GetKeyDown(InputManager.Instance.GetKey(key)))
        {
            foreach (var block in blocks)
            {
                manager.NotifySuccess(successClip);
                block.MarkSuccess();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<StartMarker>())
        {
            Debug.Log("<color=green>START!</color>");
            manager.StartMusic();
            return;
        }

        if (!manager.Active)
        {
            return;
        }

        blocks.Add(other.GetComponent<Block>());
        fillSpriteRenderer.gameObject.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!manager.Active || other.GetComponent<StartMarker>())
        {
            return;
        }

        Block block = other.GetComponent<Block>();

        if (!block.Success)
        {
            manager.NotifyFailure(failureClip);
            block.MarkFailed();
        }

        blocks.Remove(block);
    }
}
