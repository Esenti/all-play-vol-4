﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class GridGenerator : MonoBehaviour
{
    [SerializeField] private int bpm;
    [SerializeField] private float speed;
    [SerializeField] private int steps = 20;
    [SerializeField] private GameObject prefab;
    [SerializeField] private float x;
    [SerializeField] private float y;
    [SerializeField] private bool execute;

    private List<GameObject> objects = new List<GameObject>();

    void Start()
    {
    }

    void Update()
    {
        if (execute)
        {
            Debug.Log("Regenerating grid");

            for (int i = 0; i < objects.Count; ++i)
            {
                DestroyImmediate(objects[i]);
            }

            objects.Clear();

            for (int i = 0; i < steps; ++i)
            {
                objects.Add(Instantiate(prefab, transform));
            }

            float distanceInMinute = speed * 60;
            float interval = distanceInMinute / bpm;

            for (int i = 0; i < objects.Count; ++i)
            {
                objects[i].transform.position = new Vector3(x, y + interval * i, 0.0f);
            }

            execute = false;
        }
    }
}
