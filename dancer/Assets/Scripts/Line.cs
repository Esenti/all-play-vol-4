﻿using UnityEngine;

public class Line : MonoBehaviour
{
    private Rigidbody2D rigidbody;

    public Vector2 Velocity
    {
        set
        {
            rigidbody.velocity = value;
        }
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }
}
