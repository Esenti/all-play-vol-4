﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonKey : MonoBehaviour
{
    [SerializeField]
    InputManager.KeysList key;

    public InputManager.KeysList Key { get => key; set => key = value; }
}
