﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Fade : MonoBehaviour
{
    public event Action FadedIn;

    [SerializeField] private string sceneName;
    [SerializeField] private bool fadeOnStart;
    [SerializeField] private float fadeTimeStart = 0;
    [SerializeField] private GameObject narratorText;

    public void FadeOutCallback()
    {
        SceneManager.LoadScene(sceneName);
    }

    public void FadeInCallback()
    {
        FadedIn?.Invoke();
    }

    public void FadeOut()
    {
        GetComponent<Animator>().SetTrigger("Show");
    }

    public void FadeIn()
    {
        if(narratorText)
        narratorText.SetActive(false);
        GetComponent<Animator>().SetTrigger("Hide");
    }

    private void Start()
    {
        if(fadeOnStart)
        {
            Invoke("FadeIn", fadeTimeStart);
        }
    }
}
