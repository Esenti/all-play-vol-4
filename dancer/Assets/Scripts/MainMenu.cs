﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    GameObject panMain;

    [SerializeField]
    GameObject panOptions;

    [SerializeField]
    GameObject panAccessibility;

    [SerializeField]
    GameObject panCredits;

    [SerializeField]
    List<GameObject> buttons;

    void Start()
    {
        panMain.SetActive(true);
        panOptions.SetActive(false);
        panAccessibility.SetActive(false);
        panCredits.SetActive(false);
        RealoadButtons();
    }

    public void RealoadButtons()
    {
        foreach (var item in buttons)
        {
            ButtonKey buttonKey = item.GetComponent<ButtonKey>();
            Text text = item.GetComponentInChildren<Text>();

            text.text = buttonKey.Key.ToString() + " " + InputManager.Instance.GetKey(buttonKey.Key);
        }
    }

}
