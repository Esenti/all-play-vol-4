﻿using System;
using UnityEngine;

public class TargetManager : MonoBehaviour
{
    public bool Active { get; private set; }

    [SerializeField] private AudioClip failureClip;
    [SerializeField] private GameObject trackPrefab;
    [SerializeField] private Transform trackSpawnPosition;
    [SerializeField] private AudioSource music;
    [SerializeField] private Fade fade;
    [SerializeField] private float waitForBegin = 6;
    [SerializeField] private float narratorStartTime = 1;
    [SerializeField] private AudioSource narrator;
    [SerializeField] private float hardness = 3;


    private bool storyMode;
    private AudioSource audioSource;
    private float failureMeter;
    private GameObject track;

    public void NotifySuccess(AudioClip audioClip)
    {
        audioSource.PlayOneShot(audioClip);
    }

    public void NotifyFailure(AudioClip audioClip)
    {
        failureMeter += 1.0f;
        audioSource.PlayOneShot(audioClip);
    }

    private void Start()
    {
        storyMode = PlayerPrefs.GetInt("Undying", 0) == 1;

        audioSource = GetComponent<AudioSource>();
        fade.FadedIn += OnFadeIn;
        CreateTrack();


        Invoke("WaitForBegin", waitForBegin);
        Invoke("StartNarrator", narratorStartTime);

    }

    private void StartNarrator()
    {

        narrator.Play();
    }

    private void OnFadeIn()
    {
        track.GetComponent<Track>().Begin();
    }

    private void WaitForBegin()
    {
        fade.FadeIn();
    }

    void Update()
    {
        failureMeter -= Time.deltaTime;
        failureMeter = failureMeter < 0.0f ? 0.0f : failureMeter;

        if (track.GetComponent<Track>().Complete && Active)
        {
            // TODO: handle win here
            //Active = false;
            //failureMeter = 0.0f;
            //Destroy(track, 2.0f);
            //Invoke("CreateTrack", 2.0f);

            fade.FadeOut();


        }

        //Debug.Log($"<color=red>{failureMeter}</color>");

        if (!storyMode && failureMeter >= hardness)
        {
            Restart();
        }
    }

    internal void StartMusic()
    {
        music.Play();
    }

    private void CreateTrack()
    {
        track = Instantiate(trackPrefab, trackSpawnPosition.position, Quaternion.identity);
        Active = true;
    }

    private void RecreateTrack()
    {
        track = Instantiate(trackPrefab, trackSpawnPosition.position, Quaternion.identity);
        Active = true;
        track.GetComponent<Track>().Begin();
    }

    private void Restart()
    {
        Active = false;
        audioSource.PlayOneShot(failureClip);
        failureMeter = 0.0f;

        track.GetComponent<Track>().SlowDown();

        Destroy(track, 2.0f);
        Invoke("RecreateTrack", 2.0f);
    }
}
