﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputManager : Singleton<InputManager>
{

    public enum KeysList
    {
        buttonOne,
        buttonTwo,
        buttonThree,
        buttonFour,
    }
    [HideInInspector]
    [SerializeField]
    List<KeyCode> keys = new List<KeyCode>() { KeyCode.Q, KeyCode.W, KeyCode.E, KeyCode.R };

    bool waitForKey;
    int keyToWait;

    public UnityEvent keysChanged;

    private void Start()
    {
        string keysJson = PlayerPrefs.GetString("input");

        List<KeyCode> newKeys = JsonConvert.DeserializeObject<List<KeyCode>>( keysJson );
        if (newKeys.Count > 0)
            keys = newKeys;
    }

    private void Update()
    {
        if (waitForKey)
        {
            foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(kcode))
                {
                    waitForKey = false;
                    keys[keyToWait] = kcode;

                    if (keysChanged != null)
                        keysChanged.Invoke();
                }
            }

        }
    }

    public void SetKey(int key)
    {
        SetKey((KeysList)key);
    }

    public void SetKey(KeysList key)
    {
        int keyId = (int)key;
        if (keys.Count > keyId)
        {
            Invoke("SetWaitForKey", 0.1f);
            keyToWait = keyId;
            SaveInput();
        }
    }

    void SetWaitForKey()
    {

        waitForKey = true;
    }

    public KeyCode GetKey(KeysList key)
    {
        int keyId = (int)key;
        if (keys.Count > keyId)
        {
            return keys[keyId];
        }
        else
        {
            Debug.LogError("No key defined");
            return KeyCode.Escape;
        }
    }


    private void OnApplicationQuit()
    {
        SaveInput();
    }

    private void OnDestroy()
    {
        SaveInput();
    }

    private void SaveInput()
    {

       // Debug.Log(JsonConvert.SerializeObject(keys));
        PlayerPrefs.SetString("input", JsonConvert.SerializeObject(keys));
    }

}
