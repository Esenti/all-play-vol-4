﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnEnable : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnEnable()
    {
        
        EventSystem.current.SetSelectedGameObject(gameObject);
    }

    
}
