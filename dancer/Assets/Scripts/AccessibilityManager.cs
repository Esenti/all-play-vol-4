﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccessibilityManager : MonoBehaviour
{
    [SerializeField]
    private Toggle tglPreTarget;
    [SerializeField]
    private Toggle tglUndying;
    [SerializeField]
    private Toggle tglNoMush;
    [SerializeField]
    private Toggle tglBiggerTarget;

    private void Start()
    {
        tglPreTarget.isOn = PlayerPrefs.GetInt("PreTarget", 0) == 1;
        tglUndying.isOn = PlayerPrefs.GetInt("Undying", 0) == 1;
        tglNoMush.isOn = PlayerPrefs.GetInt("NoMush", 0) == 1;
        tglBiggerTarget.isOn = PlayerPrefs.GetInt("BiggerTarget", 0) == 1;
    }
    public void SetPreTarget(bool value)
    {
        PlayerPrefs.SetInt("PreTarget", value ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void SetUndying(bool value)
    {
        PlayerPrefs.SetInt("Undying", value ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void SetNoMush(bool value)
    {
        PlayerPrefs.SetInt("NoMush", value ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void SetBiggerTarget(bool value)
    {
        PlayerPrefs.SetInt("BiggerTarget", value ? 1 : 0);
        PlayerPrefs.Save();
    }
}
