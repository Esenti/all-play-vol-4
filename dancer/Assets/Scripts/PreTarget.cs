﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreTarget : MonoBehaviour
{
    [SerializeField]
    private Target target;
    [SerializeField]
    private AudioClip audioClip;


    bool on;

    private void Start()
    {
        on = PlayerPrefs.GetInt("PreTarget", 0) == 1;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!on)
            return;

        if (other.GetComponent<StartMarker>())
        {
            return;
        }
        target.PlaySound(audioClip);
    }
}
