﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public bool Done { get; private set; }
    public bool Success { get; private set; }

    public Vector2 Velocity
    {
        set
        {
            rigidbody.velocity = value;
        }
    }

    [SerializeField] SpriteRenderer fillSuccessSpriteRenderer;
    [SerializeField] SpriteRenderer fillFailureSpriteRenderer;

    private Rigidbody2D rigidbody;

    public void MarkSuccess()
    {
        Done = true;
        Success = true;
        fillSuccessSpriteRenderer.gameObject.SetActive(true);
    }

    public void MarkFailed()
    {
        Done = true;
        Success = false;
        fillFailureSpriteRenderer.gameObject.SetActive(true);
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();

    }

    void Start()
    {
        bool on = PlayerPrefs.GetInt("NoMush", 0) == 1;

        if (on && CompareTag("SmallBlock"))
        {
            Done = true;
            Success = true;
            gameObject.SetActive(false);
        }
    }
}
