﻿using System.Collections.Generic;
using UnityEngine;

public class Track : MonoBehaviour
{
    [SerializeField] private Vector2 velocity;

    private List<Block> blocks = new List<Block>();
    private List<Line> lines = new List<Line>();
    private Vector2 currentVelocity;
    private bool slowingDown;

    public bool Complete
    {
        get
        {
            foreach (var block in blocks)
            {
                if (!block.Done)
                {
                    return false;
                }
            }

            return true;
        }
    }

    public void SlowDown()
    {
        slowingDown = true;
    }

    public void Begin()
    {
        currentVelocity = velocity;

        foreach (var block in blocks)
        {
            block.Velocity = velocity;
        }

        foreach (var line in lines)
        {
            line.Velocity = velocity;
        }
    }

    private void Awake()
    {
        blocks = new List<Block>(GetComponentsInChildren<Block>());
        lines = new List<Line>(GetComponentsInChildren<Line>());
    }

    private void Start()
    {

    }

    private void Update()
    {
      

        if(slowingDown)
        {
            currentVelocity = (1.0f - Time.deltaTime) * currentVelocity;
            
            foreach (var block in blocks)
            {
                block.Velocity = currentVelocity;
            }

            foreach (var line in lines)
            {
                line.Velocity = currentVelocity;
            }
        }
    }
}
