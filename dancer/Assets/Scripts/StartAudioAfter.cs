﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAudioAfter : MonoBehaviour
{

    private AudioSource audioSource;

    [SerializeField]
    private float startAfter = 1;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayDelayed(startAfter);
    }

    
}
