﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioMixer masterMixer;
    [SerializeField]
    private Slider sldDialogs;
    [SerializeField]
    private Slider sldSoundFX;
    [SerializeField]
    private Slider sldMusic;
    [SerializeField]
    private Slider sldMaster;

    void Start()
    {
        sldDialogs.value = PlayerPrefs.GetFloat("volumeDialogs", 1);
        sldSoundFX.value = PlayerPrefs.GetFloat("volumeSoundFX", 1);
        sldMusic.value = PlayerPrefs.GetFloat("volumeMusic", 1);
        sldMaster.value = PlayerPrefs.GetFloat("volumeMaster", 1);
    }


    public void SetDialogsVolume(float decimalVolume)
    {
        var dbVolume = Mathf.Log10(decimalVolume) * 20;
        if (decimalVolume == 0.0f)
        {
            dbVolume = -80.0f;
        }
        masterMixer.SetFloat("volumeDialogs", dbVolume);
        PlayerPrefs.SetFloat("volumeDialogs", decimalVolume);

        PlayerPrefs.Save();
    }
    public void SetSoundFXVolume(float decimalVolume)
    {
        var dbVolume = Mathf.Log10(decimalVolume) * 20;
        if (decimalVolume == 0.0f)
        {
            dbVolume = -80.0f;
        }
        masterMixer.SetFloat("volumeSoundFX", dbVolume);
        PlayerPrefs.SetFloat("volumeSoundFX", decimalVolume);

        PlayerPrefs.Save();
    }
    public void SetMusicVolume(float decimalVolume)
    {
        var dbVolume = Mathf.Log10(decimalVolume) * 20;
        if (decimalVolume == 0.0f)
        {
            dbVolume = -80.0f;
        }
        masterMixer.SetFloat("volumeMusic", dbVolume);
        PlayerPrefs.SetFloat("volumeMusic", decimalVolume);

        PlayerPrefs.Save();
    }
    public void SetMasterVolume(float decimalVolume)
    {
        var dbVolume = Mathf.Log10(decimalVolume) * 20;
        if (decimalVolume == 0.0f)
        {
            dbVolume = -80.0f;
        }
        masterMixer.SetFloat("volumeMaster", dbVolume);

        PlayerPrefs.SetFloat("volumeMaster", decimalVolume);

        PlayerPrefs.Save();
    }



}
