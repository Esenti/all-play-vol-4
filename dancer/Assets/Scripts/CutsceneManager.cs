﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CutsceneManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource[] audioSources;
    [SerializeField]
    private AudioSource audioAmbient;

    [SerializeField]
    string sceneToLoadAfter;

    void LoadSceneAfter()
    {
        SceneManager.LoadScene(sceneToLoadAfter);
    }
    void StopAmbient()
    {
        audioAmbient.Stop();
    }
    
    void PlayAudioSource(int id)
    {
        foreach (var item in audioSources)
        {
            item.Stop();
        }
        audioSources[id].Play();
    }

}
